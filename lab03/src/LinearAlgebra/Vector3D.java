//Mandeep Singh 1937332

package LinearAlgebra;

public class Vector3D {

	private double x,y,z;
	
	public Vector3D(double x,double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() { return this.x;}
	public double getY() { return this.y;}
	public double getZ() { return this.z;}
	
	public double magnitude() {
		double mag =Math.sqrt(Math.pow(x, 2)+ Math.pow(y, 2) + Math.pow(z, 2));
		return mag;	
	}
 
	public double dotProduct(Vector3D v) {
		double x = v.getX();
		double y = v.getY();
		double z = v.getZ();
		
		double dP = (x*this.x)+(y*this.y)+(z*this.z);
		return dP;	
	}
	
	public Vector3D add(Vector3D v) {
		double x = v.getX() + this.x;
		double y = v.getY() + this.y;
		double z = v.getZ() + this.z;
		Vector3D v3D = new Vector3D(x,y,z);
		return v3D;	
	}
	
}
