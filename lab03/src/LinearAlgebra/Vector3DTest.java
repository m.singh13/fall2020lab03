//Mandeep Singh 1937332

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3DTest {

	@Test
	void test1() {
		Vector3D a = new Vector3D(1,2,3);
		assertEquals(1,a.getX());
		assertEquals(2,a.getY());
		assertEquals(3,a.getZ());	
	}

	@Test
	void test2() {
		Vector3D a = new Vector3D(2,4,4);
		assertEquals(6,a.magnitude());
	}
	@Test
	void test3() {
		Vector3D a = new Vector3D(1,1,2);
		Vector3D b = new Vector3D(2,3,4);
		assertEquals(13,a.dotProduct(b));
	}
	
	@Test
	void test4() {
		Vector3D a = new Vector3D(1,1,2);
		Vector3D b = new Vector3D(2,3,4);	
	    Vector3D c = b.add(a);
	    assertEquals(3,c.getX());
	    assertEquals(4,c.getY());
	    assertEquals(6,c.getZ());

	    
	}
}
